# Hiring Test Release Notes

## 2.1.1

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 2.1.0

- first publish of release notes
