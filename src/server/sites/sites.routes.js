import SitesController from "./sites.controller";

export default (server) => {
  const sitesController = new SitesController();
  server.get("/api/sites", sitesController.getSites);
  server.get("/api/sites/:idOrName", sitesController.getSite);
  server.post("/api/sites", sitesController.addSite);
  server.put("/api/sites/:id", sitesController.updateSite);
  server.delete("/api/sites/:id", sitesController.deleteSite);
  server.get("/api/statistics", sitesController.getStatistics);
};
