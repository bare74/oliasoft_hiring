import { sites } from "./sites.db";

// this is our global "db"
let sitesList = sites.slice();

export default class EmployeesController {
  constructor() {}

  getSites = (req, res) => {
    res.json(sitesList);
  };

  getSite = (req, res) => {
    const { idOrName } = req.params;
    const site = sitesList.find(
      (site) =>
        site.id === idOrName ||
        site.name.toLowerCase() === idOrName.toLowerCase()
    );
    if (site) {
      res.json(site);
    } else {
      res.status(404).json({ error: "Site not found" });
    }
  };

  addSite = (req, res) => {
    const newSite = req.body;
    sitesList.push(newSite);
    res.status(201).json(newSite);
  };

  updateSite = (req, res) => {
    const { id } = req.params;
    const updatedSite = req.body;
    const index = sitesList.findIndex((site) => site.id === id);
    if (index !== -1) {
      sitesList[index] = { ...sitesList[index], ...updatedSite };
      res.json(sitesList[index]);
    } else {
      res.status(404).json({ error: "Site not found" });
    }
  };

  deleteSite = (req, res) => {
    const { id } = req.params;
    const index = sitesList.findIndex((site) => site.id === id);
    if (index !== -1) {
      const deletedSite = sitesList[index];
      sitesList.splice(index, 1);
      res.json(deletedSite);
    } else {
      res.status(404).json({ error: "Site not found" });
    }
  };

  getStatistics = (req, res) => {
    const numberOfSites = sitesList.length;

    const numberOfOilRigs = sitesList.reduce(
      (total, site) => total + site.oilRigs.length,
      0
    );

    const numberOfNorwegianBasedSites = sitesList.filter(
      (site) => site.country === "Norway"
    ).length;

    const norwegianSites = sitesList.filter(
      (site) => site.country === "Norway"
    );
    const numberOfNorwegianBasedOilRigs = norwegianSites.reduce(
      (total, site) => total + site.oilRigs.length,
      0
    );

    const histogram = {};
    sitesList.forEach((site) => {
      const firstChar = site.name.charAt(0).toLowerCase();
      if (histogram[firstChar]) {
        histogram[firstChar]++;
      } else {
        histogram[firstChar] = 1;
      }
    });

    const statistics = {
      numberOfSites,
      numberOfOilRigs,
      numberOfNorwegianBasedSites,
      numberOfNorwegianBasedOilRigs,
      sitesPerCharacterHistogram: histogram,
    };

    res.json(statistics);
  };
}
