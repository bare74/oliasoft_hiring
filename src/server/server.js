import express from "express";
import cors from "cors";
import sitesRoutes from "./sites/sites.routes";
import oilRigsRoutes from "./oil-rigs/oil-rigs.routes";

const server = express();
const port = process.env.port || 3000;
const router = express.Router();

const corsOptions = {
  origin: "http://localhost:8080",
};
server.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", corsOptions.origin);
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
server.use(cors(corsOptions));

server.use(express.static(__dirname));

server.get("/", (req, res) => {
  res.sendFile("index.html", {
    root: __dirname,
  });
});

sitesRoutes(server);
oilRigsRoutes(server);

server.listen(port, () => {
  console.log(`Server started on port: ${port}`);
});
