import { oilRigs } from "./oil-rigs.db";

// this is our global "db"
let oilRigsList = oilRigs.slice();

export default class EmployeesController {
  constructor() {}

  getOilRigs = (req, res) => {
    res.json(oilRigsList);
  };

  getOilrigs = (req, res) => {
    const { idOrName } = req.params;
    const oilRig = oilRigsList.find(
      (oilRig) =>
        oilRig.id === idOrName ||
        oilRig.name.toLowerCase() === idOrName.toLowerCase()
    );
    if (oilRig) {
      res.json(oilRig);
    } else {
      res.status(404).json({ error: "OilRig not found" });
    }
  };

  addOilRigs = (req, res) => {
    const newOilRig = req.body;
    oilRigsList.push(newOilRig);
    res.status(201).json(newOilRig);
  };

  updateOilRigs = (req, res) => {
    const { id } = req.params;
    const updatedOilRig = req.body;
    const index = oilRigsList.findIndex((oilRig) => oilRig.id === id);
    if (index !== -1) {
      oilRigsList[index] = { ...oilRigsList[index], ...updatedOilRig };
      res.json(oilRigsList[index]);
    } else {
      res.status(404).json({ error: "OilRig not found" });
    }
  };

  deleteOilRigs = (req, res) => {
    const { id } = req.params;
    const index = oilRigsList.findIndex((oilRig) => oilRig.id === id);
    if (index !== -1) {
      const deletedOilRig = oilRigsList[index];
      oilRigsList.splice(index, 1);
      res.json(deletedOilRig);
    } else {
      res.status(404).json({ error: "OilRig not found" });
    }
  };
}
