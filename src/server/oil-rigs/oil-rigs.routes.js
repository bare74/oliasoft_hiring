import OilRigsController from "./oil-rigs.controller";

export default (server) => {
  const oilRigsController = new OilRigsController();

  server.get("/api/oil-rigs", oilRigsController.getOilRigs);
  server.get("/api/oil-rigs/:idOrName", oilRigsController.getOilRigs);
  server.post("/api/oil-rigs", oilRigsController.addOilRigs);
  server.put("/api/oil-rigs/:id", oilRigsController.updateOilRigs);
  server.delete("/api/oil-rigs/:id", oilRigsController.deleteOilRigs);
};
