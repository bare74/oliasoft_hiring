import React from "react";
import { Heading, Page } from "@oliasoft-open-source/react-ui-library";
import Sites from "../../components/sites/sites"; // Update import path
import OilRigs from "../../components/oil-rigs/oil-rigs";

export const Main = () => {
  return (
    <Page left={0}>
      <Heading top>Hiring Challenge</Heading>
      <Sites />
      <OilRigs />
    </Page>
  );
};
