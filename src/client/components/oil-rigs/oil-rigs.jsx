import React, { useState } from "react";
import { connect } from "react-redux";
import {
  Button,
  Card,
  Heading,
  Column,
  Row,
} from "@oliasoft-open-source/react-ui-library";
import { oilRigsLoaded } from "../../store/entities/oil-rigs/oil-rigs";
import styles from "./oil-rigs.module.less";
import { Link } from "react-router-dom";

const OilRigs = ({ list, loading, oilRigsLoaded }) => {
  const [sortOrder, setSortOrder] = useState("asc");
  const [showSortButton, setShowSortButton] = useState(false);

  const sortedList = [...list].sort((a, b) => {
    if (sortOrder === "asc") {
      return a.name.localeCompare(b.name);
    } else {
      return b.name.localeCompare(a.name);
    }
  });

  const handleLoadSites = () => {
    oilRigsLoaded();
    setShowSortButton(true);
  };
  return (
    <Card heading={<Heading>List of oil rigs</Heading>}>
      <Row>
        <Column width={200}>
          <Button
            label="Load oil rigs"
            onClick={handleLoadSites}
            loading={loading}
            disabled={loading}
          />
          {showSortButton && (
            <Button
              label={`Sort ${sortOrder === "asc" ? "Descending" : "Ascending"}`}
              onClick={() => {
                setSortOrder(sortOrder === "asc" ? "desc" : "asc");
              }}
            />
          )}
        </Column>
        <Column>
          <div className={styles.oilRigsList}>
            {sortedList.length ? (
              <ul>
                {sortedList.map((oilRig, i) => (
                  <li key={i}>
                    <Link to={`/oil-rigs-details/${oilRig.id}`}>
                      {oilRig.name}
                    </Link>
                  </li>
                ))}
              </ul>
            ) : (
              <em>None loaded</em>
            )}
          </div>
        </Column>
      </Row>
    </Card>
  );
};

const mapStateToProps = ({ entities }) => {
  const { oilRigs } = entities;
  return {
    loading: oilRigs.loading,
    list: oilRigs.list,
  };
};

const mapDispatchToProps = {
  oilRigsLoaded,
};

export default connect(mapStateToProps, mapDispatchToProps)(OilRigs);
