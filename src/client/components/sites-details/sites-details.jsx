import React from "react";
import { connect } from "react-redux";
import {
  Heading,
  Page,
  Card,
  Row,
  Column,
  Button,
} from "@oliasoft-open-source/react-ui-library";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import styles from "./details.module.less";
import ChartView from "../chart-view/chart-view";

const Details = ({ site }) => {
  if (!site) {
    return null;
  }

  const history = useHistory();

  return (
    <Page left={0}>
      <Card heading={<Heading>Details of Oil Sites</Heading>}>
        <Row>
          <Column width={200}>
            <Button label="Go back" onClick={() => history.push("/")} />
          </Column>
          <Column>
            <div className={styles.details}>
              <strong>
                <p>{site.name}</p>
              </strong>
              <p>{site.country}</p>
              <ul>
                {site.oilRigs.map((oilRig) => (
                  <li key={oilRig}>
                    <Link to={`/oil-rigs-details/${oilRig}`}>{oilRig}</Link>
                  </li>
                ))}
              </ul>
            </div>
          </Column>
        </Row>
      </Card>
      <ChartView />
    </Page>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { siteId } = ownProps.match.params;
  const { sites } = state.entities;
  const site = sites.list.find((site) => site.id === siteId);
  return {
    site,
  };
};

export default connect(mapStateToProps)(Details);
