import React from "react";
import { connect } from "react-redux";
import { Bar } from "react-chartjs-2";
import {
  Card,
  Heading,
  Row,
  Column,
  Button,
} from "@oliasoft-open-source/react-ui-library";
import { useHistory } from "react-router-dom";

const ChartView = ({ sites }) => {
  const siteNames = sites.map((site) => site.name);
  const oilRigAmounts = sites.map((site) => site.oilRigs.length);

  const history = useHistory();

  const goBack = () => {
    history.goBack();
  };

  const chartData = {
    labels: siteNames,
    datasets: [
      {
        label: "Oil Rig Amount",
        data: oilRigAmounts,
        backgroundColor: "rgb(255,103,0)",
      },
    ],
  };
  const chartOptions = {
    scales: {
      y: {
        beginAtZero: true,
        title: {
          display: true,
          text: "Amount of Oil Rigs",
        },
      },
      x: {
        title: {
          display: true,
          text: "Site",
        },
      },
    },
  };

  return (
    <Card heading={<Heading>Oil Rig Amounts by Site</Heading>}>
      <Row>
        <Column width={200}>
          <Button label="Go back" onClick={goBack} />
        </Column>
        <Column>
          <Bar data={chartData} options={chartOptions} />
        </Column>
      </Row>
    </Card>
  );
};

const mapStateToProps = ({ entities }) => {
  const { sites } = entities;
  return {
    sites: sites.list,
  };
};

export default connect(mapStateToProps)(ChartView);
