import React, { useState } from "react";
import { connect } from "react-redux";
import {
  Button,
  Card,
  Heading,
  Column,
  Row,
} from "@oliasoft-open-source/react-ui-library";
import { sitesLoaded } from "~store/entities/sites/sites";
import styles from "./sites.module.less";
import { Link } from "react-router-dom";

const Sites = ({ list, loading, sitesLoaded }) => {
  const [sortOrder, setSortOrder] = useState("asc");
  const [showSortButton, setShowSortButton] = useState(false);

  const sortedList = [...list].sort((a, b) => {
    if (sortOrder === "asc") {
      return a.name.localeCompare(b.name);
    } else {
      return b.name.localeCompare(a.name);
    }
  });

  const handleLoadSites = () => {
    sitesLoaded();
    setShowSortButton(true);
  };

  return (
    <Card heading={<Heading>List of oil sites</Heading>}>
      <Row>
        <Column width={200}>
          <Button
            label="Load sites"
            onClick={handleLoadSites}
            loading={loading}
            disabled={loading}
          />
          {showSortButton && (
            <Button
              label={`Sort ${sortOrder === "asc" ? "Descending" : "Ascending"}`}
              onClick={() => {
                setSortOrder(sortOrder === "asc" ? "desc" : "asc");
              }}
            />
          )}
        </Column>
        <Column>
          <div className={styles.sitesList}>
            {sortedList.length ? (
              <ul>
                {sortedList.map((site, i) => (
                  <li key={i}>
                    <strong>
                      <Link to={`/details/${site.id}`}>{site.name}</Link>
                    </strong>
                    <p>{site.country}</p>
                  </li>
                ))}
              </ul>
            ) : (
              <em>None loaded</em>
            )}
          </div>
        </Column>
      </Row>
    </Card>
  );
};

const mapStateToProps = ({ entities }) => {
  const { sites } = entities;
  return {
    loading: sites.loading,
    list: sites.list,
  };
};

const mapDispatchToProps = {
  sitesLoaded,
};

export default connect(mapStateToProps, mapDispatchToProps)(Sites);
