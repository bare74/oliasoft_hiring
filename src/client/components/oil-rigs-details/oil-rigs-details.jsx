import React from "react";
import { connect } from "react-redux";
import {
  Card,
  Heading,
  Row,
  Column,
  Page,
  Button,
} from "@oliasoft-open-source/react-ui-library";
import { useHistory } from "react-router-dom";
import styles from "./oil.rigs.details.module.less";

const OilRigsDetails = ({ oilRig }) => {
  if (!oilRig) {
    return (
      <div className={styles.oilrigsdetails}>
        <strong>
          <p>Oil rig details not available or loading</p>
        </strong>
      </div>
    );
  }

  const history = useHistory();

  const goBack = () => {
    history.goBack();
  };

  return (
    <Page left={0}>
      <Card heading={<Heading>Details of Oil Rigs</Heading>}>
        <Row>
          <Column width={200}>
            <Button label="Go back" onClick={goBack} />
          </Column>
          <Column>
            <strong>
              <p>{oilRig.name}</p>
            </strong>
            <p>Manufacturer: {oilRig.manufacturer}</p>
            <p>ID: {oilRig.id}</p>
          </Column>
        </Row>
      </Card>
    </Page>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { oilRigId } = ownProps.match.params;
  const { oilRigs } = state.entities;
  const oilRig = oilRigs.list.find((oilRig) => oilRig.id === oilRigId);

  return {
    oilRig,
  };
};

export default connect(mapStateToProps)(OilRigsDetails);
