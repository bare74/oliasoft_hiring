import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { TopBar } from "@oliasoft-open-source/react-ui-library";
import Logo from "../../images/logo@2x.png";
import { Main } from "../../views/main/main";
import SiteDetails from "../../components/sites-details/sites-details";
import OilRigsDetails from "../../components/oil-rigs-details/oil-rigs-details"; // Import the OilRigsDetails component
import chartView from "../../components/chart-view/chart-view";

export const Routes = () => {
  return (
    <>
      <TopBar
        title={{
          logo: <img src={Logo} alt="logo" />,
          label: "Hiring Challenge",
        }}
      />
      <Router>
        <Route path="/" exact component={Main} />
        <Route path="/details/:siteId" component={SiteDetails} />
        <Route path="/oil-rigs-details/:oilRigId" component={OilRigsDetails} />
        <Route path="/chart" component={chartView} />
      </Router>
    </>
  );
};
